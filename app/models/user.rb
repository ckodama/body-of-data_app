class User < ActiveRecord::Base
  validates :name, presence: true, length: { maximum: 20 }
  validates :height, presence: true, length: { maximum: 3 }
  validates :weight, presence: true, length: { maximum: 3 }
  validates :complications, presence: true
  
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
